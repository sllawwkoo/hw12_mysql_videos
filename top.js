const { useKnex } = require("./base");
const { cliOptions } = require("./cliOptions");

const top = async () => {
	try {
		const { knex } = await useKnex();

		const result = await knex("videos")
			.select("category")
			.sum("views as viewsCount")
			.groupBy("category")
			.orderBy("viewsCount", "desc")
			.limit(cliOptions.top);

		console.table(
			result.map((row) => ({ Category: row.category, Views: row.viewsCount }))
		);
	} catch (err) {
		console.error(err);
	}
};

(async () => {
	console.log(await top());
})();

// node top --top 2