const { useKnex } = require("./base");
const { cliOptions } = require("./cliOptions");

const paginate = async () => {
	try {
		const { knex } = await useKnex();

		const { page, size } = cliOptions;
		const offset = (page - 1) * size;

		const videos = await knex("videos").select("*").limit(size).offset(offset);

		console.table(videos);
	} catch (err) {
		console.error(err);
	}
};


(async () => {
	console.table(await paginate());
})();

//  node paginate --page 2 --size 3