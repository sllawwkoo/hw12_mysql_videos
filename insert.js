const { useKnex } = require("./base");
const {cliOptions} = require("./cliOptions");

const insert = async () => {
	try {
		const { knex } = await useKnex();
		await knex("videos").insert({
			title: cliOptions.title,
			views: cliOptions.views,
			category: cliOptions.category,
			created_at: new Date(),
			updated_at: new Date(),
		});
		console.log('Insert complete!');
	} catch (err) {
		console.log(err);
	}
};

(async () => {
	console.log(await insert());
})();

//node insert --title 'Great news' --views 14 --category 'news'