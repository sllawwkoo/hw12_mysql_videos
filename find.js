const { useKnex } = require("./base");
const { cliOptions } = require("./cliOptions");

const find = async () => {
	try {
		const { knex } = await useKnex();

		const { search } = cliOptions;

		const videos = await knex("videos")
			.select("*")
			.where("title", "like", `%${search}%`);

		console.table(videos);
	} catch (err) {
		console.error(err);
	}
};

(async () => {
	console.table(await find());
})();

//node find --search text