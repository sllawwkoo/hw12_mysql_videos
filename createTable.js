const { useKnex } = require("./base");
const createTable = async () => {
	return new Promise(async (resolve, reject) => {
		const { knex } = await useKnex();

		knex.schema
			.createTable("videos", (table) => {
				table.increments("id").primary();
				table.string("title");
				table.float("views");
				table.string("category");
				table.timestamps("created_at");
				
				resolve("Table 'videos' created!");
			}).catch((err) => {
				reject(err);
			});
	});
};

(async () => {
	console.log(await createTable());
})()
