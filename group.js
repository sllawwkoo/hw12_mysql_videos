const { useKnex } = require("./base");

const group = async () => {
	try {
		const { knex } = await useKnex();

		const result = await knex("videos")
			.select("category")
			.sum("views as views_count")
			.groupBy("category");

		result.forEach((row) => {
			console.table(`${row.category} - ${row.views_count}`);
		});
	} catch (err) {
		console.error(err);
	}
};

(async () => {
	console.table(await group());
})();