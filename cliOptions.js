const comandLineArgs = require("command-line-args");

const optionsDefinitions = [
	{ name: "id", type: Number },
	{ name: "title", type: String },
	{ name: "views", type: Number },
	{ name: "category", type: String },
	{ name: "page", type: Number },
	{ name: "size", type: Number },
	{ name: "search", type: String },
	{ name: "top", type: Number },
];

const cliOptions = comandLineArgs(optionsDefinitions);

module.exports = {
	cliOptions,
};
